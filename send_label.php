<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);

const MAX_LABELS_PER_DAY = 10;
const DB_CONNECTION = 'mysql:host=localhost;dbname=buyback_data';
const DB_USERNAME = 'm2bwhole69590821';
const DB_PASSWORD = 'Chicha2020@';

if ($_SERVER['REQUEST_METHOD'] != 'POST') {
    return;
}

require __DIR__ . '/includes/helpers.php';

$response = [
    'success' => false,
    'message' => '',
];

$recaptcha_solved = check_recaptcha();

if (!$recaptcha_solved) {
    $response['success'] = false;
    $response['message'] = 'Error. Invalid captcha. Please, try again';
    echo json_encode($response);
    return;
}

$form = [];
$form['company'] = htmlentities($_POST['company']);
$form['name'] = htmlentities($_POST['name']);
$form['addressLine1'] = htmlentities($_POST['addressLine1']);
$form['addressLine2'] = htmlentities($_POST['addressLine2']);
$form['zipCode'] = htmlentities($_POST['zipCode']);
$form['city'] = htmlentities($_POST['city']);
$form['stateProvinceCode'] = htmlentities($_POST['stateProvinceCode']);
$form['country'] = htmlentities($_POST['country']);
$form['email'] = htmlentities($_POST['email']);
$form['phone'] = htmlentities($_POST['phone']);
$form['boxes_quantity'] = intval(htmlentities($_POST['boxes_quantity']));
$form['quantity'] = [
    'iphone_x'       => '0',
    'iphone_8_plus'  => '0',
    'iphone_8'       => '0',
    'iphone_7_plus'  => '0',
    'iphone_7'       => '0',
    'iphone_6s_plus' => '0',
    'iphone_6s'      => '0',
    'iphone_6_plus'  => '0',
    'iphone_6'       => '0',
    'iphone_5'       => '0',
];
$form['total_quantity'] = 0;
foreach ($_POST['quantity'] as $iphone_model => $screens_quantity) {
    $clean_iphone_model = htmlentities($iphone_model);
    $clean_quantity = intval(htmlentities($screens_quantity));

    $form['quantity'][$clean_iphone_model] = $clean_quantity;
    if ($clean_quantity && $clean_quantity > 0) {
        $form['total_quantity'] += $clean_quantity;
    }
}

if ($form['total_quantity'] <= 0) {
    $response['success'] = false;
    $response['message'] = 'Error. Please, specify screens quantity';
    echo json_encode($response);
    return;
}

$approve_token = null;
$is_approved = true;
if ($form['boxes_quantity'] > MAX_LABELS_PER_DAY) {
    $is_approved = false;
} else {
    try {
        $dbh = new PDO(DB_CONNECTION, DB_USERNAME, DB_PASSWORD);

        $today_printed = 0;
        foreach ($dbh->query('SELECT count(id) as `count` FROM shipping_labels WHERE DATE(created_at) = CURDATE()') as $row) {
            $today_printed += $row['count'];
        }

        if ($today_printed + $form['boxes_quantity'] > MAX_LABELS_PER_DAY) {
            $is_approved = false;
        }
    } catch (\Exception $e) {
        $error_dump = var_export($e, true);

        $parameters = [
            'to'         => [ 'Aatradeusa@yahoo.com' ],
            'bcc'        => [ 'alexzh1998@gmail.com' ],
            'subject'    => 'Unknown Error at Getting Shipping Labels Count from Database ' . date("d.m.Y H:i"),
            'body'       => include 'includes/emails/email_database_error.php',
        ];
        send_email_swiftmailer($parameters);

        $is_approved = false;
    }
}

if ($is_approved) {
    require __DIR__ . '/vendor/autoload.php';

    $ups_username = 'M2BWholesale';
    $ups_account_number = '11W15V';
    $ups_password = 'Chicha12';
    $ups_access_key = '8D3968C9389E5868';
    // To address
    $my_profile['email'] = 'aatradeusa@yahoo.com';
    $my_profile['name'] = 'M2Bwholesale.com';
    $my_profile['company'] = 'M2Bwholesale.com';
    $my_profile['addressLine1'] = '17105 North Bay Road B505';
    $my_profile['addressLine2'] = '';
    $my_profile['zipCode'] = '33160';
    $my_profile['city'] = 'Sunny Isles Beach';
    $my_profile['stateProvinceCode'] = 'FL';
    $my_profile['country'] = 'US';
    $my_profile['phone'] = '786 7753 9937';

    // Start shipment
    $shipment = new Ups\Entity\Shipment;

    // Set shipper
    $shipper = $shipment->getShipper();
    $shipper->setShipperNumber($ups_account_number);
    $shipper->setName($form['company']);
    $shipper->setAttentionName($form['name']);
    $shipperAddress = $shipper->getAddress();
    $shipperAddress->setAddressLine1($form['addressLine1']);
    if ($form['addressLine2']) {
        $shipperAddress->setAddressLine2($form['addressLine2']);
    }
    $shipperAddress->setPostalCode($form['zipCode']);
    $shipperAddress->setCity($form['city']);
    $shipperAddress->setStateProvinceCode($form['stateProvinceCode']);
    $shipperAddress->setCountryCode($form['country']);
    $shipper->setAddress($shipperAddress);
    $shipper->setEmailAddress($form['email']);
    $shipper->setPhoneNumber($form['phone']);
    $shipment->setShipper($shipper);
    // From address
    $address = new \Ups\Entity\Address();
    $address->setAddressLine1($form['addressLine1']);
    if ($form['addressLine2']) {
        $address->setAddressLine2($form['addressLine2']);
    }
    $address->setPostalCode($form['zipCode']);
    $address->setCity($form['city']);
    $address->setStateProvinceCode($form['stateProvinceCode']);
    $address->setCountryCode($form['country']);
    $shipFrom = new \Ups\Entity\ShipFrom();
    $shipFrom->setAddress($address);
    $shipFrom->setName($form['name']);
    $shipFrom->setAttentionName($shipFrom->getName());
    $shipFrom->setCompanyName($shipFrom->getName());
    $shipFrom->setEmailAddress($form['email']);
    $shipFrom->setPhoneNumber($form['phone']);
    $shipment->setShipFrom($shipFrom);

    // To address
    $address = new \Ups\Entity\Address();
    $address->setAddressLine1($my_profile['addressLine1']);
    $address->setPostalCode($my_profile['zipCode']);
    $address->setCity($my_profile['city']);
    $address->setStateProvinceCode($my_profile['stateProvinceCode']);
    $address->setCountryCode($my_profile['country']);
    $shipTo = new \Ups\Entity\ShipTo();
    $shipTo->setAddress($address);
    $shipTo->setCompanyName($my_profile['company']);
    $shipTo->setAttentionName($my_profile['name']);
    $shipTo->setEmailAddress($my_profile['email']);
    $shipTo->setPhoneNumber($my_profile['phone']);
    $shipment->setShipTo($shipTo);

    // Set service
    $service = new \Ups\Entity\Service;
    $service->setCode(\Ups\Entity\Service::S_GROUND);
    $service->setDescription($service->getName());
    $shipment->setService($service);

    // Set description
    $shipment->setDescription('Broken phone screens');

    $ounces_per_box = $form['total_quantity'] / $form['boxes_quantity']; // 1 screen = 1 ounce
    for ($i = 1; $i <= $form['boxes_quantity']; $i++) {
        // Create Package
        $package = new \Ups\Entity\Package();

        $package->getPackagingType()->setCode(\Ups\Entity\PackagingType::PT_PACKAGE);
        $unit = new \Ups\Entity\UnitOfMeasurement;
        if ($form['country'] == 'US') {
            $package->getPackageWeight()->setWeight($ounces_per_box / 16);
            $unit->setCode(\Ups\Entity\UnitOfMeasurement::UOM_LBS);
        } else {
            $package->getPackageWeight()->setWeight($ounces_per_box / 0.0283495);
            $unit->setCode(\Ups\Entity\UnitOfMeasurement::UOM_KGS);
        }
        $package->getPackageWeight()->setUnitOfMeasurement($unit);

        // Set dimensions
        $dimensions = new \Ups\Entity\Dimensions();
        $unit = new \Ups\Entity\UnitOfMeasurement;
        $ounces = $form['total_quantity'];
        if ($form['country'] == 'US') {
            $dimensions->setWidth(6);
            $dimensions->setHeight(8);
            $dimensions->setLength(6);
            $unit->setCode(\Ups\Entity\UnitOfMeasurement::UOM_IN);
        } else {
            $dimensions->setWidth(15.24);
            $dimensions->setHeight(20.32);
            $dimensions->setLength(15.24);
            $unit->setCode(\Ups\Entity\UnitOfMeasurement::UOM_CM);
        };
        $dimensions->setUnitOfMeasurement($unit);
        $package->setDimensions($dimensions);
        // Add descriptions because it is a package
        $package->setDescription('Broken screens');
        // Add this package
        $shipment->addPackage($package);
    }

    //// Set Reference Number
    //$referenceNumber = new \Ups\Entity\ReferenceNumber;
    //if ($return) {
    //    $referenceNumber->setCode(\Ups\Entity\ReferenceNumber::CODE_RETURN_AUTHORIZATION_NUMBER);
    //    $referenceNumber->setValue($return_id);
    //} else {
    //    $referenceNumber->setCode(\Ups\Entity\ReferenceNumber::CODE_INVOICE_NUMBER);
    //    $referenceNumber->setValue($order_id);
    //}
    //$shipment->setReferenceNumber($referenceNumber);

    // Set payment information
    $shipment->setPaymentInformation(new \Ups\Entity\PaymentInformation(\Ups\Entity\PaymentInformation::TYPE_PREPAID, (object)array('AccountNumber' => $ups_account_number)));

    // Ask for negotiated rates (optional)
    $rateInformation = new \Ups\Entity\RateInformation;
    $rateInformation->setNegotiatedRatesIndicator(1);
    $shipment->setRateInformation($rateInformation);

    $error_dump = false;
    try {
        $api = new Ups\Shipping($ups_access_key, $ups_username, $ups_password);

        $confirm = $api->confirm(\Ups\Shipping::REQ_VALIDATE, $shipment);

        if ($confirm) {
            $accept = $api->accept($confirm->ShipmentDigest);

            $label_images = [];
            $attachments = [];

            $response['success'] = true;
            $response['approved'] = true;
            $response['message'] = 'Shipping label was sent to your email address';
            $response['imagesBase64'] = [];
            $response['screensData'] = [];
            $response['trackingNumbers'] = [];

            foreach ($form['quantity'] as $iphone_model => $screens_quantity) {
                if ($screens_quantity && $screens_quantity > 0) {
                    $response['screensData'][] = [
                        'screenModel' => $iphone_model,
                        'screensQuantity' => $screens_quantity,
                    ];
                }
            }

            $packageResults = [];
            if (is_array($accept->PackageResults)) {
                $packageResults = $accept->PackageResults;
            } else {
                $packageResults[] = $accept->PackageResults;
            }
            foreach ($packageResults as $packageResult) {
                $label_image = base64_decode($packageResult->LabelImage->GraphicImage);
                $label_image_path = 'labels/' . $packageResult->TrackingNumber . '_' . uniqid() . '.gif';
                file_put_contents($label_image_path, $label_image);

                $label_images[] = [
                    'data' => $label_image,
                    'path' => $label_image_path,
                    'tracking_number' => $packageResult->TrackingNumber,
                ];

                $response['imagesBase64'][] = 'data:image/gif;base64,' . $packageResult->LabelImage->GraphicImage;
                $response['trackingNumbers'][] = $packageResult->TrackingNumber;

                $attachments[] = new Swift_Attachment($label_image, $packageResult->TrackingNumber . '.gif', 'image/gif');
            }

            $shipment_info = '';
            $shipment_info .= sprintf('Transportation Charges: %s %s<br/>', $accept->ShipmentCharges->TransportationCharges->MonetaryValue, $accept->ShipmentCharges->TransportationCharges->CurrencyCode);
            $shipment_info .= sprintf('Service Options Charges: %s %s<br/>', $accept->ShipmentCharges->ServiceOptionsCharges->MonetaryValue, $accept->ShipmentCharges->ServiceOptionsCharges->CurrencyCode);
            $shipment_info .= sprintf('Total Charges: %s %s <br/><br/>', $accept->ShipmentCharges->TotalCharges->MonetaryValue, $accept->ShipmentCharges->TotalCharges->CurrencyCode);
            $shipment_info .= sprintf('Weight: : %s %s<br/><br/>', $accept->BillingWeight->Weight, $accept->BillingWeight->UnitOfMeasurement->Description);
            $shipment_info .= sprintf('Shipment Identification Number: %s<br/>', $accept->ShipmentIdentificationNumber);

            // Send email to customer with shipping label
            $parameters = [
                'to'         => [ $form['email'] ],
                'subject'    => 'Your FREE Shipping Label ' . date("d.m.Y H:i"),
                'body'       => include 'includes/emails/email_customer_label_requested.php',
                'attachments' => $attachments,
            ];
            send_email_swiftmailer($parameters);

            // Notify Customer Service about new shipping label request
            $parameters = [
                'to'         => [ 'Aatradeusa@yahoo.com' ],
                'subject'    => 'New Shipping Label Request ' . date("d.m.Y H:i"),
                'body'       => include 'includes/emails/email_customer_service_label_requested.php',
                'attachments' => $attachments,
            ];
            send_email_swiftmailer($parameters);
        } else {
            $error_dump = var_export($confirm, true);
            $parameters = [
                'to'         => [ 'Aatradeusa@yahoo.com' ],
                'bcc'        => [ 'alexzh1998@gmail.com' ],
                'subject'    => 'Error Occured at Requesting Shipping Label ' . date("d.m.Y H:i"),
                'body'       => include 'includes/emails/email_customer_service_label_request_error.php',
            ];
            send_email_swiftmailer($parameters);

            $response['success'] = false;
            $response['message'] = 'Error occured. Customer service is already notified, they will contact you as soon as possible.';
        }
    } catch (\Exception $e) {
        $send_error_email = false;
        $response['success'] = false;

        $send_error_email = false;
        if ($e->getMessage() == 'Failure: ShipFrom PhoneNumber must be at least 10 alphanumeric characters (120313)') {
            $response['message'] = 'Error. Invalid phone number. Please, try again';
        } elseif ($e->getMessage() == 'Failure: ShipFrom phone number cannot be more than 15 digits long (120318)') {
            $response['message'] = 'Error. Invalid phone number. Please, try again';
        } elseif ($e->getMessage() == 'Failure: Missing/Invalid Shipper PostalCode. Postal Code length must be 6. (120107)') {
            $response['message'] = 'Error. Invalid postal code. Please, try again';
        } elseif ($e->getMessage() == 'Failure: Shipper EmailAddress is an invalid format (120112)') {
            $response['message'] = 'Error. Invalid email. Please, try again';
        } elseif ($e->getMessage() == 'Failure: The selected service is not available from the origin to the destination. (121210)') {
            $response['message'] = 'It is unable to print shipping label for your country instantly. Customer service is already notified, they will contact you as soon as possible.';
            $send_error_email = true;
        }  elseif ($e->getMessage() == 'Failure: Address Validation Error on Shipper address (120801)') {
            $response['message'] = 'It was unable to validate your address. Please, try again. Customer service is already notified, they will contact you as soon as possible.';
            $send_error_email = true;
        } else {
            $response['message'] = 'Unknown error. Customer service is already notified, they will contact you as soon as possible.';
            $send_error_email = true;
        }

        if ($send_error_email) {
            $error_dump = var_export($e, true);
            $parameters = [
                'to'         => [ 'Aatradeusa@yahoo.com' ],
                'bcc'        => [ 'alexzh1998@gmail.com' ],
                'subject'    => 'Unknown Error at Requesting Shipping Label ' . date("d.m.Y H:i"),
                'body'       => include 'includes/emails/email_customer_service_label_request_error.php',
            ];
            send_email_swiftmailer($parameters);
        }
    }
} else {
    $approve_token = md5(uniqid() . 'hello m2bw');

    $approve_url = $_SERVER['HTTP_HOST'] . '/j4hfo98467/ok_approve.php?kg48nf02n24dxcvVd=' . $approve_token;

    $parameters = [
        'to'         => [ 'Aatradeusa@yahoo.com' ],
        'subject'    => 'Shipping Labels Need Your Approve ' . date("d.m.Y H:i"),
        'body'       => include 'includes/emails/email_customer_service_label_approve_request.php',
    ];
    send_email_swiftmailer($parameters);

    $response['success'] = true;
    $response['approved'] = false;
}

// Send JSON response
echo json_encode($response);

try {
    $dbh = new PDO(DB_CONNECTION, DB_USERNAME, DB_PASSWORD);

    $insert_shipping_stmt = $dbh->prepare('INSERT INTO shippings
    (
        name, company, email, phone, address_line_1, address_line_2, zip_code, city, state, country, 
        approved, approve_token, identification_number, boxes_quantity,
        iphone_x, iphone_8_plus, iphone_8, iphone_7_plus, iphone_7,
        iphone_6s_plus, iphone_6s, iphone_6_plus, iphone_6, iphone_5,
        total_quantity
    )
    VALUES
    (
        :name, :company, :email, :phone, :address_line_1, :address_line_2, :zip_code, :city, :state, :country,
        :approved, :approve_token, :identification_number, :boxes_quantity,
        :iphone_x, :iphone_8_plus, :iphone_8, :iphone_7_plus, :iphone_7,
        :iphone_6s_plus, :iphone_6s, :iphone_6_plus, :iphone_6, :iphone_5,
        :total_quantity
    )');
    $insert_shipping_stmt->bindParam(':name', $form['name']);
    $insert_shipping_stmt->bindParam(':company', $form['company']);
    $insert_shipping_stmt->bindParam(':email', $form['email']);
    $insert_shipping_stmt->bindParam(':phone', $form['phone']);
    $insert_shipping_stmt->bindParam(':address_line_1', $form['addressLine1']);
    $insert_shipping_stmt->bindParam(':address_line_2', $form['addressLine2']);
    $insert_shipping_stmt->bindParam(':zip_code', $form['zipCode']);
    $insert_shipping_stmt->bindParam(':city', $form['city']);
    $insert_shipping_stmt->bindParam(':state', $form['stateProvinceCode']);
    $insert_shipping_stmt->bindParam(':country', $form['country']);
    $insert_shipping_stmt->bindParam(':approved', $is_approved);
    $insert_shipping_stmt->bindParam(':approve_token', $approve_token);
    if (isset($accept)) {
        $indentification_number = $accept->ShipmentIdentificationNumber;
    } else {
        $indentification_number = null;
    }
    $insert_shipping_stmt->bindParam(':identification_number', $indentification_number);
    $insert_shipping_stmt->bindParam(':boxes_quantity', $form['boxes_quantity']);
    $insert_shipping_stmt->bindParam(':iphone_x', $form['quantity']['iphone_x']);
    $insert_shipping_stmt->bindParam(':iphone_8_plus', $form['quantity']['iphone_8_plus']);
    $insert_shipping_stmt->bindParam(':iphone_8', $form['quantity']['iphone_8']);
    $insert_shipping_stmt->bindParam(':iphone_7_plus', $form['quantity']['iphone_7_plus']);
    $insert_shipping_stmt->bindParam(':iphone_7', $form['quantity']['iphone_7']);
    $insert_shipping_stmt->bindParam(':iphone_6s_plus', $form['quantity']['iphone_6s_plus']);
    $insert_shipping_stmt->bindParam(':iphone_6s', $form['quantity']['iphone_6s']);
    $insert_shipping_stmt->bindParam(':iphone_6_plus', $form['quantity']['iphone_6_plus']);
    $insert_shipping_stmt->bindParam(':iphone_6', $form['quantity']['iphone_6']);
    $insert_shipping_stmt->bindParam(':iphone_5', $form['quantity']['iphone_5']);
    $insert_shipping_stmt->bindParam(':total_quantity', $form['total_quantity']);
    $insert_shipping_stmt->execute();

    $inserted_shipping_id = $dbh->lastInsertId();

    if (isset($label_images)) {
        $insert_shipping_label_stmt = $dbh->prepare('INSERT INTO shipping_labels (shipping_id, tracking_number, img) VALUES (:shipping_id, :tracking_number, :img)');
        $insert_shipping_label_stmt->bindParam(':shipping_id', $inserted_shipping_id);

        foreach ($label_images as $label_image) {
            $label_image_path = '/' . $label_image['path'];
            $insert_shipping_label_stmt->bindParam(':img', $label_image_path);
            $insert_shipping_label_stmt->bindParam(':tracking_number', $label_image['tracking_number']);
            $insert_shipping_label_stmt->execute();
        }
    }
} catch (\Exception $e) {
    $error_dump = var_export($e, true);
    $parameters = [
        'to'         => [ 'Aatradeusa@yahoo.com' ],
        'bcc'        => [ 'alexzh1998@gmail.com' ],
        'subject'    => 'Unknown Error at Saving Shipping Data to Database ' . date("d.m.Y H:i"),
        'body'       => include 'includes/emails/email_database_error.php',
    ];
    send_email_swiftmailer($parameters);
}
