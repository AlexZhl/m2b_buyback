<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>We Pay Top Dollar For Broken LCD Screens for iPhone - M2Bwholesale</title>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="/css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="/css/style.css" rel="stylesheet">
    <!--  Favicon package by realfavicongenerator.net  -->
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#023b52">
    <meta name="theme-color" content="#ffffff">
</head>
<body>
<!--Main Navigation-->
<header>

    <!--Navbar-->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar">
        <div class="container">
            <a href="#">
                <img src="/img/m2b_logo.png" alt="" style="height: 2.5rem;">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-7"
                    aria-controls="navbarSupportedContent-7" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent-7">
                <ul class="navbar-nav ml-auto smooth-scroll">
                    <li class="nav-item">
                        <a class="nav-link" href="#pricelist">Prices</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#about-us">About us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#how-to">How It Works</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#testimonials">Reviews</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#contact-us">Contact</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="#pricelist">Get Shipping Label</a>
                    </li>
                </ul>


            </div>
        </div>
    </nav>

    <!--Intro Section-->
    <section class="view intro-2 hm-gradient">
        <div class="full-bg-img">
            <div class="container flex-center">
                <div class="row flex-center pt-5 mt-3">
                    <div class="col-md-6 text-center text-md-left margins">
                        <div class="white-text smooth-scroll">
                            <h1 class="h1-responsive wow fadeInLeft" data-wow-delay="0.3s">We Pay Top Dollar For Broken
                                LCD Screens for iPhone</h1>
                            <hr class="hr-light wow fadeInLeft" data-wow-delay="0.3s">
                            <h6 class="wow fadeInLeft" data-wow-delay="0.3s">
                                We buy LCD screens worldwide for high prices. <br/>
                                You just need to print shipping label, ship screens to us and get your money.</h6>
                            <br>
                            <a href="#pricelist" class="btn btn-outline-white wow fadeInLeft" data-wow-delay="0.3s">
                                Prices
                                <i class="fa fa-dollar" aria-hidden="true"></i>
                            </a>
                            <a href="#pricelist" class="btn btn-outline-white wow fadeInLeft" data-wow-delay="0.3s">
                                Print shipping label
                                <i class="fa fa-truck" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>

                    <div class="col-md-6 wow fadeInRight d-flex justify-content-center" data-wow-delay="0.3s">
                        <img src="/img/broken-iphone.png" alt="" class="img-responsive">
                    </div>
                </div>
            </div>
        </div>
    </section>

</header>
<!--Main Navigation-->

<!--Main Layout-->
<main class="pt-5">
    <div class="container">
        <!--Grid row-->
        <section id="pricelist" class="pb-5">
            <!-- Heading -->
            <h2 class="section-heading h1 pt-4 mb-5 text-center">Broken LCD's Prices</h2>

            <div class="row">

            </div>            <div class="row">

            </div>            <div class="row">

            </div>
            <div class="row">
                <form action="send_label.php" id="form-screens" method="post" class="form-full-width">
                    <!--Card-->
                    <div class="col-md-12 text-center">
                        <div class="card card-cascade wider reverse my-4">
                            <!--Card image-->
                            <div class="view overlay hm-white-slight">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th class="custom-color-x-border custom-color-x"></th>
                                        <th>Model</th>
                                        <th colspan="2">Price</th>
                                        <th>Quantity</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td rowspan="3" class="custom-color-x-border custom-color-x"></td>
                                        <td class="w-25" rowspan="3"><b>iPhone X</b></td>
                                        <td class="custom-color-x">
                                            OEM
                                            <a tabindex="0" class="popover-enable"
                                               role="button" data-toggle="popover" data-html="true" data-trigger="hover"
                                               title="What is that?"
                                               data-content="Original LCD which has not been repaired before">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                        <td class="custom-color-x">$45</td>
                                        <td rowspan="3" class="td-with-input">
                                            <div class="md-form">
                                                <input id="quantity-iphone-x" type="text" name="quantity[iphone_x]" class="form-control calculated">
                                                <label for="quantity-iphone-x">Type quantity here</label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Minor Issues
                                            <a tabindex="0" class="popover-enable"
                                               role="button" data-toggle="popover" data-html="true" data-trigger="hover"
                                               title="What is that?"
                                               data-content="- Backlight issue <br/> - LCD pressure light yellow <br/> - 3D Touch not working <br/> - Tiny dead pixel <br/> - Light leakage, shadow">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                        <td>$10</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Bad
                                            <a tabindex="0" class="popover-enable"
                                               role="button" data-toggle="popover" data-html="true" data-trigger="hover"
                                               title="What is that?"
                                               data-content="- Cracked dead lcd <br/> - Bleeding spot">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                        <td>$1</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="card card-cascade wider reverse my-4">
                            <!--Card image-->
                            <div class="view overlay hm-white-slight">
                                <table class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <td rowspan="3" class="custom-color-border custom-color"></td>
                                        <td class="w-25" rowspan="7"><b>iPhone 8 Plus</b></td>
                                        <td class="custom-color">
                                            OEM
                                            <a tabindex="0" class="popover-enable"
                                               role="button" data-toggle="popover" data-html="true" data-trigger="hover"
                                               title="What is that?"
                                               data-content="Original LCD which has not been repaired before">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                        <td class="custom-color">$50</td>
                                        <td rowspan="3" class="td-with-input">
                                            <div class="md-form">
                                                <input id="quantity-iphone-8-plus" type="text" name="quantity[iphone_8_plus]" class="form-control calculated">
                                                <label for="quantity-iphone-8-plus">Type quantity here</label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Minor Issues
                                            <a tabindex="0" class="popover-enable"
                                               role="button" data-toggle="popover" data-html="true" data-trigger="hover"
                                               title="What is that?"
                                               data-content="- Backlight issue <br/> - LCD pressure light yellow <br/> - 3D Touch not working <br/> - Tiny dead pixel <br/> - Light leakage, shadow">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                        <td>$10</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Bad
                                            <a tabindex="0" class="popover-enable"
                                               role="button" data-toggle="popover" data-html="true" data-trigger="hover"
                                               title="What is that?"
                                               data-content="- Cracked dead lcd <br/> - Bleeding spot">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                        <td>$1</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="card card-cascade wider reverse my-4">
                            <!--Card image-->
                            <div class="view overlay hm-white-slight">
                                <table class="table table-bordered">
                                    <tbody>

                                    <tr>
                                        <td rowspan="7" class="custom-color-border custom-color"></td>
                                        <td class="w-25" rowspan="7"><b>iPhone 8</b></td>
                                        <td class="custom-color">
                                            OEM
                                            <a tabindex="0" class="popover-enable"
                                               role="button" data-toggle="popover" data-html="true" data-trigger="hover"
                                               title="What is that?"
                                               data-content="Original LCD which has not been repaired before">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                            </a></td>
                                        <td class="custom-color">$40</td>
                                        <td rowspan="7" class="td-with-input">
                                            <div class="md-form">
                                                <input id="quantity-iphone-8" type="text" name="quantity[iphone_8]" class="form-control calculated">
                                                <label for="quantity-iphone-8">Type quantity here</label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Minor Issues
                                            <a tabindex="0" class="popover-enable"
                                               role="button" data-toggle="popover" data-html="true" data-trigger="hover"
                                               title="What is that?"
                                               data-content="- Backlight issue <br/> - LCD pressure light yellow <br/> - 3D Touch not working <br/> - Tiny dead pixel <br/> - Light leakage, shadow">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                        <td>$10</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Bad
                                            <a tabindex="0" class="popover-enable"
                                               role="button" data-toggle="popover" data-html="true" data-trigger="hover"
                                               title="What is that?"
                                               data-content="- Cracked dead lcd <br/> - Bleeding spot">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                        <td>$1</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="card card-cascade wider reverse my-4">
                            <!--Card image-->
                            <div class="view overlay hm-white-slight">
                                <table class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <td rowspan="7" class="custom-color-border custom-color"></td>
                                        <td class="w-25" rowspan="7"><b>iPhone 7 Plus</b></td>
                                        <td class="custom-color">
                                            OEM
                                            <a tabindex="0" class="popover-enable"
                                               role="button" data-toggle="popover" data-html="true" data-trigger="hover"
                                               title="What is that?"
                                               data-content="Original LCD which has not been repaired before">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                            </a></td>
                                        <td class="custom-color">$40</td>
                                        <td rowspan="7" class="td-with-input">
                                            <div class="md-form">
                                                <input id="quantity-iphone-7-plus" type="text" name="quantity[iphone_7_plus]" class="form-control calculated">
                                                <label for="quantity-iphone-7-plus">Type quantity here</label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Minor Issues
                                            <a tabindex="0" class="popover-enable"
                                               role="button" data-toggle="popover" data-html="true" data-trigger="hover"
                                               title="What is that?"
                                               data-content="- Backlight issue <br/> - LCD pressure light yellow <br/> - 3D Touch not working <br/> - Tiny dead pixel <br/> - Light leakage, shadow">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                        <td>$15</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Bad
                                            <a tabindex="0" class="popover-enable"
                                               role="button" data-toggle="popover" data-html="true" data-trigger="hover"
                                               title="What is that?"
                                               data-content="- Cracked dead lcd <br/> - Bleeding spot">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                        <td>$1.50</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="card card-cascade wider reverse my-4">
                            <!--Card image-->
                            <div class="view overlay hm-white-slight">
                                <table class="table table-bordered">
                                    <tbody>

                                    <tr>
                                        <td rowspan="7" class="custom-color-border custom-color"></td>
                                        <td class="w-25" rowspan="7"><b>iPhone 7</b></td>
                                        <td class="custom-color">
                                            OEM
                                            <a tabindex="0" class="popover-enable"
                                               role="button" data-toggle="popover" data-html="true" data-trigger="hover"
                                               title="What is that?"
                                               data-content="Original LCD which has not been repaired before">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                            </a></td>
                                        <td class="custom-color">$25</td>
                                        <td rowspan="7" class="td-with-input">
                                            <div class="md-form">
                                                <input id="quantity-iphone-7" type="text" name="quantity[iphone_7]" class="form-control calculated">
                                                <label for="quantity-iphone-7">Type quantity here</label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Minor Issues
                                            <a tabindex="0" class="popover-enable"
                                               role="button" data-toggle="popover" data-html="true" data-trigger="hover"
                                               title="What is that?"
                                               data-content="- Backlight issue <br/> - LCD pressure light yellow <br/> - 3D Touch not working <br/> - Tiny dead pixel <br/> - Light leakage, shadow">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                        <td>$10</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Bad
                                            <a tabindex="0" class="popover-enable"
                                               role="button" data-toggle="popover" data-html="true" data-trigger="hover"
                                               title="What is that?"
                                               data-content="- Cracked dead lcd <br/> - Bleeding spot">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                        <td>$1.50</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="card card-cascade wider reverse my-4">
                            <!--Card image-->
                            <div class="view overlay hm-white-slight">
                                <table class="table table-bordered">
                                    <tbody>

                                    <tr>
                                        <td rowspan="7" class="custom-color-border custom-color"></td>
                                        <td class="w-25" rowspan="7"><b>iPhone 6S Plus</b></td>
                                        <td class="custom-color">
                                            OEM
                                            <a tabindex="0" class="popover-enable"
                                               role="button" data-toggle="popover" data-html="true" data-trigger="hover"
                                               title="What is that?"
                                               data-content="Original LCD which has not been repaired before">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                            </a></td>
                                        <td class="custom-color">$25</td>
                                        <td rowspan="7" class="td-with-input">
                                            <div class="md-form">
                                                <input id="quantity-iphone-6s-plus" type="text" name="quantity[iphone_6s_plus]" class="form-control calculated">
                                                <label for="quantity-iphone-6s-plus">Type quantity here</label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Minor Issues
                                            <a tabindex="0" class="popover-enable"
                                               role="button" data-toggle="popover" data-html="true" data-trigger="hover"
                                               title="What is that?"
                                               data-content="- Backlight issue <br/> - LCD pressure light yellow <br/> - 3D Touch not working <br/> - Tiny dead pixel <br/> - Light leakage, shadow">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                        <td>$10</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Bad
                                            <a tabindex="0" class="popover-enable"
                                               role="button" data-toggle="popover" data-html="true" data-trigger="hover"
                                               title="What is that?"
                                               data-content="- Cracked dead lcd <br/> - Bleeding spot">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                        <td>$1</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="card card-cascade wider reverse my-4">
                            <!--Card image-->
                            <div class="view overlay hm-white-slight">
                                <table class="table table-bordered">
                                    <tbody>

                                    <tr>
                                        <td rowspan="7" class="custom-color-border custom-color"></td>
                                        <td class="w-25" rowspan="7"><b>iPhone 6S</b></td>
                                        <td class="custom-color">
                                            OEM
                                            <a tabindex="0" class="popover-enable"
                                               role="button" data-toggle="popover" data-html="true" data-trigger="hover"
                                               title="What is that?"
                                               data-content="Original LCD which has not been repaired before">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                            </a></td>
                                        <td class="custom-color">$13</td>
                                        <td rowspan="7" class="td-with-input">
                                            <div class="md-form">
                                                <input id="quantity-iphone-6s" type="text" name="quantity[iphone_6s]" class="form-control calculated">
                                                <label for="quantity-iphone-6s">Type quantity here</label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Minor Issues
                                            <a tabindex="0" class="popover-enable"
                                               role="button" data-toggle="popover" data-html="true" data-trigger="hover"
                                               title="What is that?"
                                               data-content="- Backlight issue <br/> - LCD pressure light yellow <br/> - 3D Touch not working <br/> - Tiny dead pixel <br/> - Light leakage, shadow">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                        <td>$6</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Bad
                                            <a tabindex="0" class="popover-enable"
                                               role="button" data-toggle="popover" data-html="true" data-trigger="hover"
                                               title="What is that?"
                                               data-content="- Cracked dead lcd <br/> - Bleeding spot">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                        <td>$1</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="card card-cascade wider reverse my-4">
                            <!--Card image-->
                            <div class="view overlay hm-white-slight">
                                <table class="table table-bordered">
                                    <tbody>

                                    <tr>
                                        <td rowspan="6" class="custom-color-border custom-color"></td>
                                        <td class="w-25" rowspan="6"><b>iPhone 6 Plus</b></td>
                                        <td class="custom-color">
                                            OEM
                                            <a tabindex="0" class="popover-enable"
                                               role="button" data-toggle="popover" data-html="true" data-trigger="hover"
                                               title="What is that?"
                                               data-content="Original LCD which has not been repaired before">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                            </a></td>
                                        <td class="custom-color">$17</td>
                                        <td rowspan="6" class="td-with-input">
                                            <div class="md-form">
                                                <input id="quantity-iphone-6-plus" type="text" name="quantity[iphone_6_plus]" class="form-control calculated">
                                                <label for="quantity-iphone-6-plus">Type quantity here</label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Minor Issues
                                            <a tabindex="0" class="popover-enable"
                                               role="button" data-toggle="popover" data-html="true" data-trigger="hover"
                                               title="What is that?"
                                               data-content="- Backlight issue <br/> - LCD pressure light yellow <br/> - 3D Touch not working <br/> - Tiny dead pixel <br/> - Light leakage, shadow">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                        <td>$5</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Bad
                                            <a tabindex="0" class="popover-enable"
                                               role="button" data-toggle="popover" data-html="true" data-trigger="hover"
                                               title="What is that?"
                                               data-content="- Cracked dead lcd <br/> - Bleeding spot">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                        <td>$0.10</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="card card-cascade wider reverse my-4">
                            <!--Card image-->
                            <div class="view overlay hm-white-slight">
                                <table class="table table-bordered">
                                    <tbody>

                                    <tr>
                                        <td rowspan="6" class="custom-color-border custom-color"></td>
                                        <td class="w-25" rowspan="6"><b>iPhone 6</b></td>
                                        <div class="custom-color">
                                            <td class="">
                                                OEM
                                                <a tabindex="0" class="popover-enable"
                                                   role="button" data-toggle="popover" data-html="true" data-trigger="hover"
                                                   title="What is that?"
                                                   data-content="Original LCD which has not been repaired before">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                </a></td>
                                            <td class="">$9</td>
                                        </div>

                                        <td rowspan="6" class="td-with-input">
                                            <div class="md-form">
                                                <input id="quantity-iphone-6" type="text" name="quantity[iphone_6]" class="form-control calculated">
                                                <label for="quantity-iphone-6">Type quantity here</label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Minor Issues
                                            <a tabindex="0" class="popover-enable"
                                               role="button" data-toggle="popover" data-html="true" data-trigger="hover"
                                               title="What is that?"
                                               data-content="- Backlight issue <br/> - LCD pressure light yellow <br/> - 3D Touch not working <br/> - Tiny dead pixel <br/> - Light leakage, shadow">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                        <td>$2</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Bad
                                            <a tabindex="0" class="popover-enable"
                                               role="button" data-toggle="popover" data-html="true" data-trigger="hover"
                                               title="What is that?"
                                               data-content="- Cracked dead lcd <br/> - Bleeding spot">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                        <td>$0.10</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="card card-cascade wider reverse my-4">
                            <!--Card image-->
                            <div class="view overlay hm-white-slight">
                                <table class="table table-bordered">
                                    <tbody>

                                    <tr>
                                        <td rowspan="6" class="custom-color-border custom-color"></td>
                                        <td class="w-25" rowspan="6"><b>iPhone 5 series</b></td>
                                        <td class="custom-color">
                                            OEM
                                            <a tabindex="0" class="popover-enable"
                                               role="button" data-toggle="popover" data-html="true" data-trigger="hover"
                                               title="What is that?"
                                               data-content="Original LCD which has not been repaired before">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                            </a></td>
                                        <td class="custom-color">$1</td>
                                        <td rowspan="6" class="td-with-input">
                                            <div class="md-form">
                                                <input id="quantity-iphone-5" type="text" name="quantity[iphone_5]" class="form-control calculated">
                                                <label for="quantity-iphone-5">Type quantity here</label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Minor Issues
                                            <a tabindex="0" class="popover-enable"
                                               role="button" data-toggle="popover" data-html="true" data-trigger="hover"
                                               title="What is that?"
                                               data-content="- Backlight issue <br/> - LCD pressure light yellow <br/> - 3D Touch not working <br/> - Tiny dead pixel <br/> - Light leakage, shadow">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                        <td>$0.50</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Bad
                                            <a tabindex="0" class="popover-enable"
                                               role="button" data-toggle="popover" data-html="true" data-trigger="hover"
                                               title="What is that?"
                                               data-content="- Cracked dead lcd <br/> - Bleeding spot">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                        <td>$0.05</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!--/Card image-->

                            <!--Card content-->
                            <div class="card-body text-center mt-4">
                                <h5 class="deep-orange-text"><strong id="total-quantity">Total quantity: 0 screens</strong></h5>

                                <button id="openModal" type="button" class="btn peach-gradient btn-lg">Print Shipping Label
                                </button>
                            </div>
                            <!--/.Card content-->

                        </div>
                    </div>

                    <div id="boxShippingInfo" class="col-md-8 mx-auto" style="display: none;">
                        <div class="mt-5 card card-cascade narrower">
                            <div class="view gradient-card-header deep-orange text-center">
                                <h2 class="h2-responsive"><i class="fa fa-truck"></i> Shipping details</h2>
                            </div>

                            <div class="card-body">
                                <div class="row">
                                    <div id="not-qualified-for-free-shipping" class="col-md-12 mb-3">
                                        <div class="alert alert-warning">
                                            <i class="fa fa-warning prefix"></i>
                                            You are not qualified for free shipping. You need at least 50 screens.
                                            You still can get this label, but shipping fee would be deducted from your total payout.
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="md-form form-sm">
                                            <i class="fa fa-user prefix"></i>
                                            <input name="name" type="text" id="form1" class="form-control" required maxlength="35">
                                            <label for="form1">Your name</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="md-form form-sm">
                                            <i class="fa fa-suitcase prefix"></i>
                                            <input name="company" type="text" id="form2" class="form-control" required maxlength="35">
                                            <label for="form2">Company</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="md-form form-sm">
                                            <i class="fa fa-building prefix"></i>
                                            <input name="addressLine1" type="text" id="form3" class="form-control" required maxlength="35">
                                            <label for="form3">Address line 1</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="md-form form-sm">
                                            <i class="fa fa-building prefix"></i>
                                            <input name="addressLine2" type="text" id="form4" class="form-control" maxlength="35">
                                            <label for="form4">Address line 2</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="md-form form-sm">
                                            <i class="fa fa-building prefix"></i>
                                            <input name="zipCode" type="text" id="form5" class="form-control" required maxlength="9">
                                            <label for="form5">Zip Code</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="md-form form-sm">
                                            <i class="fa fa-building prefix"></i>
                                            <input name="city" type="text" id="form6" class="form-control" required maxlength="30">
                                            <label for="form6">City</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="md-form form-sm" id="region-input-div">
                                            <i class="fa fa-building deep-orange-text prefix"></i>
                                            <select id="usa-states-select" name="stateProvinceCode" id="form7" class="mdb-select colorful-select dropdown-warning" required>
                                                <option value="AL">Alabama</option>
                                                <option value="AK">Alaska</option>
                                                <option value="AZ">Arizona</option>
                                                <option value="AR">Arkansas</option>
                                                <option value="CA">California</option>
                                                <option value="CO">Colorado</option>
                                                <option value="CT">Connecticut</option>
                                                <option value="DE">Delaware</option>
                                                <option value="DC">District Of Columbia</option>
                                                <option value="FL">Florida</option>
                                                <option value="GA">Georgia</option>
                                                <option value="HI">Hawaii</option>
                                                <option value="ID">Idaho</option>
                                                <option value="IL">Illinois</option>
                                                <option value="IN">Indiana</option>
                                                <option value="IA">Iowa</option>
                                                <option value="KS">Kansas</option>
                                                <option value="KY">Kentucky</option>
                                                <option value="LA">Louisiana</option>
                                                <option value="ME">Maine</option>
                                                <option value="MD">Maryland</option>
                                                <option value="MA">Massachusetts</option>
                                                <option value="MI">Michigan</option>
                                                <option value="MN">Minnesota</option>
                                                <option value="MS">Mississippi</option>
                                                <option value="MO">Missouri</option>
                                                <option value="MT">Montana</option>
                                                <option value="NE">Nebraska</option>
                                                <option value="NV">Nevada</option>
                                                <option value="NH">New Hampshire</option>
                                                <option value="NJ">New Jersey</option>
                                                <option value="NM">New Mexico</option>
                                                <option value="NY">New York</option>
                                                <option value="NC">North Carolina</option>
                                                <option value="ND">North Dakota</option>
                                                <option value="OH">Ohio</option>
                                                <option value="OK">Oklahoma</option>
                                                <option value="OR">Oregon</option>
                                                <option value="PA">Pennsylvania</option>
                                                <option value="RI">Rhode Island</option>
                                                <option value="SC">South Carolina</option>
                                                <option value="SD">South Dakota</option>
                                                <option value="TN">Tennessee</option>
                                                <option value="TX">Texas</option>
                                                <option value="UT">Utah</option>
                                                <option value="VT">Vermont</option>
                                                <option value="VA">Virginia</option>
                                                <option value="WA">Washington</option>
                                                <option value="WV">West Virginia</option>
                                                <option value="WI">Wisconsin</option>
                                                <option value="WY">Wyoming</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="md-form form-sm">
                                            <i class="fa fa-building deep-orange-text prefix"></i>
                                            <select name="country" id="form8" class="mdb-select colorful-select dropdown-warning" required>
                                                <option value="US">United States</option>
                                                <option value="AF">Afghanistan</option>
                                                <option value="AX">Åland Islands</option>
                                                <option value="AL">Albania</option>
                                                <option value="DZ">Algeria</option>
                                                <option value="AS">American Samoa</option>
                                                <option value="AD">Andorra</option>
                                                <option value="AO">Angola</option>
                                                <option value="AI">Anguilla</option>
                                                <option value="AQ">Antarctica</option>
                                                <option value="AG">Antigua and Barbuda</option>
                                                <option value="AR">Argentina</option>
                                                <option value="AM">Armenia</option>
                                                <option value="AW">Aruba</option>
                                                <option value="AU">Australia</option>
                                                <option value="AT">Austria</option>
                                                <option value="AZ">Azerbaijan</option>
                                                <option value="BS">Bahamas</option>
                                                <option value="BH">Bahrain</option>
                                                <option value="BD">Bangladesh</option>
                                                <option value="BB">Barbados</option>
                                                <option value="BY">Belarus</option>
                                                <option value="BE">Belgium</option>
                                                <option value="BZ">Belize</option>
                                                <option value="BJ">Benin</option>
                                                <option value="BM">Bermuda</option>
                                                <option value="BT">Bhutan</option>
                                                <option value="BO">Bolivia, Plurinational State of</option>
                                                <option value="BQ">Bonaire, Sint Eustatius and Saba</option>
                                                <option value="BA">Bosnia and Herzegovina</option>
                                                <option value="BW">Botswana</option>
                                                <option value="BV">Bouvet Island</option>
                                                <option value="BR">Brazil</option>
                                                <option value="IO">British Indian Ocean Territory</option>
                                                <option value="BN">Brunei Darussalam</option>
                                                <option value="BG">Bulgaria</option>
                                                <option value="BF">Burkina Faso</option>
                                                <option value="BI">Burundi</option>
                                                <option value="KH">Cambodia</option>
                                                <option value="CM">Cameroon</option>
                                                <option value="CA">Canada</option>
                                                <option value="CV">Cape Verde</option>
                                                <option value="KY">Cayman Islands</option>
                                                <option value="CF">Central African Republic</option>
                                                <option value="TD">Chad</option>
                                                <option value="CL">Chile</option>
                                                <option value="CN">China</option>
                                                <option value="CX">Christmas Island</option>
                                                <option value="CC">Cocos (Keeling) Islands</option>
                                                <option value="CO">Colombia</option>
                                                <option value="KM">Comoros</option>
                                                <option value="CG">Congo</option>
                                                <option value="CD">Congo, the Democratic Republic of the</option>
                                                <option value="CK">Cook Islands</option>
                                                <option value="CR">Costa Rica</option>
                                                <option value="CI">Côte d'Ivoire</option>
                                                <option value="HR">Croatia</option>
                                                <option value="CU">Cuba</option>
                                                <option value="CW">Curaçao</option>
                                                <option value="CY">Cyprus</option>
                                                <option value="CZ">Czech Republic</option>
                                                <option value="DK">Denmark</option>
                                                <option value="DJ">Djibouti</option>
                                                <option value="DM">Dominica</option>
                                                <option value="DO">Dominican Republic</option>
                                                <option value="EC">Ecuador</option>
                                                <option value="EG">Egypt</option>
                                                <option value="SV">El Salvador</option>
                                                <option value="GQ">Equatorial Guinea</option>
                                                <option value="ER">Eritrea</option>
                                                <option value="EE">Estonia</option>
                                                <option value="ET">Ethiopia</option>
                                                <option value="FK">Falkland Islands (Malvinas)</option>
                                                <option value="FO">Faroe Islands</option>
                                                <option value="FJ">Fiji</option>
                                                <option value="FI">Finland</option>
                                                <option value="FR">France</option>
                                                <option value="GF">French Guiana</option>
                                                <option value="PF">French Polynesia</option>
                                                <option value="TF">French Southern Territories</option>
                                                <option value="GA">Gabon</option>
                                                <option value="GM">Gambia</option>
                                                <option value="GE">Georgia</option>
                                                <option value="DE">Germany</option>
                                                <option value="GH">Ghana</option>
                                                <option value="GI">Gibraltar</option>
                                                <option value="GR">Greece</option>
                                                <option value="GL">Greenland</option>
                                                <option value="GD">Grenada</option>
                                                <option value="GP">Guadeloupe</option>
                                                <option value="GU">Guam</option>
                                                <option value="GT">Guatemala</option>
                                                <option value="GG">Guernsey</option>
                                                <option value="GN">Guinea</option>
                                                <option value="GW">Guinea-Bissau</option>
                                                <option value="GY">Guyana</option>
                                                <option value="HT">Haiti</option>
                                                <option value="HM">Heard Island and McDonald Islands</option>
                                                <option value="VA">Holy See (Vatican City State)</option>
                                                <option value="HN">Honduras</option>
                                                <option value="HK">Hong Kong</option>
                                                <option value="HU">Hungary</option>
                                                <option value="IS">Iceland</option>
                                                <option value="IN">India</option>
                                                <option value="ID">Indonesia</option>
                                                <option value="IR">Iran, Islamic Republic of</option>
                                                <option value="IQ">Iraq</option>
                                                <option value="IE">Ireland</option>
                                                <option value="IM">Isle of Man</option>
                                                <option value="IL">Israel</option>
                                                <option value="IT">Italy</option>
                                                <option value="JM">Jamaica</option>
                                                <option value="JP">Japan</option>
                                                <option value="JE">Jersey</option>
                                                <option value="JO">Jordan</option>
                                                <option value="KZ">Kazakhstan</option>
                                                <option value="KE">Kenya</option>
                                                <option value="KI">Kiribati</option>
                                                <option value="KP">Korea, Democratic People's Republic of</option>
                                                <option value="KR">Korea, Republic of</option>
                                                <option value="KW">Kuwait</option>
                                                <option value="KG">Kyrgyzstan</option>
                                                <option value="LA">Lao People's Democratic Republic</option>
                                                <option value="LV">Latvia</option>
                                                <option value="LB">Lebanon</option>
                                                <option value="LS">Lesotho</option>
                                                <option value="LR">Liberia</option>
                                                <option value="LY">Libya</option>
                                                <option value="LI">Liechtenstein</option>
                                                <option value="LT">Lithuania</option>
                                                <option value="LU">Luxembourg</option>
                                                <option value="MO">Macao</option>
                                                <option value="MK">Macedonia, the former Yugoslav Republic of</option>
                                                <option value="MG">Madagascar</option>
                                                <option value="MW">Malawi</option>
                                                <option value="MY">Malaysia</option>
                                                <option value="MV">Maldives</option>
                                                <option value="ML">Mali</option>
                                                <option value="MT">Malta</option>
                                                <option value="MH">Marshall Islands</option>
                                                <option value="MQ">Martinique</option>
                                                <option value="MR">Mauritania</option>
                                                <option value="MU">Mauritius</option>
                                                <option value="YT">Mayotte</option>
                                                <option value="MX">Mexico</option>
                                                <option value="FM">Micronesia, Federated States of</option>
                                                <option value="MD">Moldova, Republic of</option>
                                                <option value="MC">Monaco</option>
                                                <option value="MN">Mongolia</option>
                                                <option value="ME">Montenegro</option>
                                                <option value="MS">Montserrat</option>
                                                <option value="MA">Morocco</option>
                                                <option value="MZ">Mozambique</option>
                                                <option value="MM">Myanmar</option>
                                                <option value="NA">Namibia</option>
                                                <option value="NR">Nauru</option>
                                                <option value="NP">Nepal</option>
                                                <option value="NL">Netherlands</option>
                                                <option value="NC">New Caledonia</option>
                                                <option value="NZ">New Zealand</option>
                                                <option value="NI">Nicaragua</option>
                                                <option value="NE">Niger</option>
                                                <option value="NG">Nigeria</option>
                                                <option value="NU">Niue</option>
                                                <option value="NF">Norfolk Island</option>
                                                <option value="MP">Northern Mariana Islands</option>
                                                <option value="NO">Norway</option>
                                                <option value="OM">Oman</option>
                                                <option value="PK">Pakistan</option>
                                                <option value="PW">Palau</option>
                                                <option value="PS">Palestinian Territory, Occupied</option>
                                                <option value="PA">Panama</option>
                                                <option value="PG">Papua New Guinea</option>
                                                <option value="PY">Paraguay</option>
                                                <option value="PE">Peru</option>
                                                <option value="PH">Philippines</option>
                                                <option value="PN">Pitcairn</option>
                                                <option value="PL">Poland</option>
                                                <option value="PT">Portugal</option>
                                                <option value="PR">Puerto Rico</option>
                                                <option value="QA">Qatar</option>
                                                <option value="RE">Réunion</option>
                                                <option value="RO">Romania</option>
                                                <option value="RU">Russian Federation</option>
                                                <option value="RW">Rwanda</option>
                                                <option value="BL">Saint Barthélemy</option>
                                                <option value="SH">Saint Helena, Ascension and Tristan da Cunha</option>
                                                <option value="KN">Saint Kitts and Nevis</option>
                                                <option value="LC">Saint Lucia</option>
                                                <option value="MF">Saint Martin (French part)</option>
                                                <option value="PM">Saint Pierre and Miquelon</option>
                                                <option value="VC">Saint Vincent and the Grenadines</option>
                                                <option value="WS">Samoa</option>
                                                <option value="SM">San Marino</option>
                                                <option value="ST">Sao Tome and Principe</option>
                                                <option value="SA">Saudi Arabia</option>
                                                <option value="SN">Senegal</option>
                                                <option value="RS">Serbia</option>
                                                <option value="SC">Seychelles</option>
                                                <option value="SL">Sierra Leone</option>
                                                <option value="SG">Singapore</option>
                                                <option value="SX">Sint Maarten (Dutch part)</option>
                                                <option value="SK">Slovakia</option>
                                                <option value="SI">Slovenia</option>
                                                <option value="SB">Solomon Islands</option>
                                                <option value="SO">Somalia</option>
                                                <option value="ZA">South Africa</option>
                                                <option value="GS">South Georgia and the South Sandwich Islands</option>
                                                <option value="SS">South Sudan</option>
                                                <option value="ES">Spain</option>
                                                <option value="LK">Sri Lanka</option>
                                                <option value="SD">Sudan</option>
                                                <option value="SR">Suriname</option>
                                                <option value="SJ">Svalbard and Jan Mayen</option>
                                                <option value="SZ">Swaziland</option>
                                                <option value="SE">Sweden</option>
                                                <option value="CH">Switzerland</option>
                                                <option value="SY">Syrian Arab Republic</option>
                                                <option value="TW">Taiwan, Province of China</option>
                                                <option value="TJ">Tajikistan</option>
                                                <option value="TZ">Tanzania, United Republic of</option>
                                                <option value="TH">Thailand</option>
                                                <option value="TL">Timor-Leste</option>
                                                <option value="TG">Togo</option>
                                                <option value="TK">Tokelau</option>
                                                <option value="TO">Tonga</option>
                                                <option value="TT">Trinidad and Tobago</option>
                                                <option value="TN">Tunisia</option>
                                                <option value="TR">Turkey</option>
                                                <option value="TM">Turkmenistan</option>
                                                <option value="TC">Turks and Caicos Islands</option>
                                                <option value="TV">Tuvalu</option>
                                                <option value="UG">Uganda</option>
                                                <option value="UA">Ukraine</option>
                                                <option value="AE">United Arab Emirates</option>
                                                <option value="GB">United Kingdom</option>
                                                <option value="UM">United States Minor Outlying Islands</option>
                                                <option value="UY">Uruguay</option>
                                                <option value="UZ">Uzbekistan</option>
                                                <option value="VU">Vanuatu</option>
                                                <option value="VE">Venezuela, Bolivarian Republic of</option>
                                                <option value="VN">Viet Nam</option>
                                                <option value="VG">Virgin Islands, British</option>
                                                <option value="VI">Virgin Islands, U.S.</option>
                                                <option value="WF">Wallis and Futuna</option>
                                                <option value="EH">Western Sahara</option>
                                                <option value="YE">Yemen</option>
                                                <option value="ZM">Zambia</option>
                                                <option value="ZW">Zimbabwe</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="md-form form-sm">
                                            <i class="fa fa-envelope prefix"></i>
                                            <input name="email" type="text" id="form9" class="form-control" required>
                                            <label for="form9">Email</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="md-form form-sm">
                                            <i class="fa fa-phone prefix"></i>
                                            <input name="phone" type="text" id="form10" class="form-control" required maxlength="15">
                                            <label for="form10">Phone number</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6 mx-auto">
                                        <div class="md-form form-sm">
                                            <i class="fa fa-cubes prefix"></i>
                                            <input name="boxes_quantity" type="text" id="form11" class="form-control" required maxlength="3">
                                            <label for="form11">How many boxes will you use?</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="text-center mt-1-half">
                                    <div id='screens-recaptcha' class="g-recaptcha"
                                         data-callback="sendScreensFormAjax"
                                         data-size="invisible"></div>

                                    <button class="btn btn-deep-orange mb-1">Print <i class="fa fa-print ml-1"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
        <!--Grid row-->
    </div>
    <div class="modal fade" id="modalConfirmPrint" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm modal-notify modal-success" role="document">
            <div class="modal-content text-center">
                <div class="modal-header d-flex justify-content-center">
                    <p class="heading">Success</p>
                </div>

                <div class="modal-body">

                    <i class="fa fa-print fa-4x mb-3 animated rotateIn"></i>
                    <p>
                        Thank you.
                        <br/>
                        Shipping label was created successfully. A copy was sent to your email.
                        <br/>
                        <br/>
                        Do you want to print it now?
                    </p>
                </div>

                <div class="modal-footer flex-center">
                    <a id="confirm-print" class="btn btn-outline-secondary-modal">Yes</a>
                    <a type="button" class="btn btn-primary-modal waves-effect" data-dismiss="modal">No</a>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalNotApproved" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg cascading-modal" role="document">
            <div class="modal-content">
                <div class="modal-header deep-orange white-text">
                    <h4 class="title"><i class="fa fa-search"></i> Under Review</h4>
                    <button type="button" class="close waves-effect waves-light" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body mb-0 text-center">
                    <div class="row">
                        <div class="col-md-12">
                            <i class="fa fa-search fa-5x mb-3 animated rotateIn"></i>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            Labels limit per day reached. For security reasons we have to hold your request until
                            approval from our sale persons. Usually it takes less than an hour if we are online.
                            Your shipping labels will be emailed to you right after approval.
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            We are really sorry for any inconvience caused.
                        </div>
                    </div>

                    <div class="text-center mt-1-half">
                        <button class="btn btn-deep-orange mb-1" data-dismiss="modal">OK</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="about-us" class="container-fluid background-grey">
        <div class="container py-5">
            <!--Grid row-->
            <section>
                <!-- Heading -->
                <h2 class="section-heading h1 pt-3 mb-5 text-center">About Us</h2>

                <!--Grid row-->
                <div class="row">
                    <!--Grid column-->
                    <div class="col-md-6">
                        <img src="/img/about-us.jpg"
                             alt="placeholder" class="img-thumbnail">
                    </div>
                    <!--Grid column-->

                    <!--Grid column-->
                    <div class="col-md-6">

<!--                        <h4 class="mb-3"><strong>We are great company, definitely</strong></h4>-->
                        <p style="font-size: 1.2rem;">
                            M2Bwholesale Inc was founded in 2016 and it's becoming the fastest broken
                            lcd collecting company in the United States. We work with each client one on
                            one and provide the highest value. Our goal is to provide repair shops with highest
                            prices on their cracked working lcds.
                        </p>
                        <p style="font-size: 1.2rem;">
                            Our M2B recycling programs offer our Clients most money for their broken screens.
                            We strive to provide excellent service to our clients throughout the entire program.
                        </p>
                    </div>
                    <!--Grid column-->

                </div>
                <!--Grid row-->
            </section>
            <!--Grid row-->
        </div>
    </div>

    <div class="container pb-5">
        <section id="how-to" class="section">
            <!--Section heading-->
            <h2 class="section-heading h1 pt-4 font-bold wow fadeIn" data-wow-delay="0.2s">How It Works?</h2>

            <!-- Timeline -->
            <div class="row mt-5">
                <div class="col-md-12">

                    <!-- Timeline Wrapper -->
                    <ul class="stepper stepper-vertical colorful-timeline pl-0">

                        <li>
                            <!--Section Title -->
                            <a href="#!">
                                <span class="circle deep-orange z-depth-1-half"><i class="fa fa-sticky-note-o"
                                                                                   aria-hidden="true"></i></span>
                            </a>

                            <!-- Section Description -->
                            <div class="step-content z-depth-1 ml-xl-0 p-0 mt-2">
                                <h4 class="font-bold timeline-header deep-orange white-text p-3 mb-0">
                                    Prepare and Package your broken screens
                                </h4>
                                <p class="mb-0 p-4">
                                    Please carefully package all the screens to ship to us. When packaging
                                    the screens, make sure they are well protected and boxed well.
                                </p>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <!--Section Title -->
                            <a href="#!">
                                <span class="circle deep-orange z-depth-1-half"><i class="fa fa-truck"
                                                                                   aria-hidden="true"></i></span>
                            </a>

                            <!-- Section Description -->
                            <div class="step-content z-depth-1 ml-xl-0 p-0 mt-2">
                                <h4 class="font-bold timeline-header deep-orange white-text p-3 mb-0">Ship to us</h4>
                                <p class="mb-0 p-4">
                                    We provide free shipping label when reached 50 lcds quantity at a time.
                                    You may request a FREE shipping label through our website when you have 50pcs
                                    or more. If you are arranging for shipping on your own, ship to the address in our
                                    footer and make sure that you include your name, contact number and email address).
                                </p>
                            </div>
                        </li>
                        <li>
                            <!--Section Title -->
                            <a href="#!">
                                <span class="circle deep-orange z-depth-1-half"><i class="fa fa-clipboard"
                                                                                   aria-hidden="true"></i></span>
                            </a>

                            <!-- Section Description -->
                            <div class="step-content z-depth-1 ml-xl-0 p-0 mt-2">
                                <h4 class="font-bold timeline-header deep-orange white-text p-3 mb-0">Testing Results</h4>
                                <p class="mb-0 p-4">
                                    Upon reviving your screens we will test your screens and
                                    provide a detailed report within 24-48 hours.
                                </p>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <!--Section Title -->
                            <a href="#!">
                                <span class="circle deep-orange z-depth-1-half"><i class="fa fa-dollar"
                                                                                   aria-hidden="true"></i></span>
                            </a>

                            <!-- Section Description -->
                            <div class="step-content z-depth-1 ml-xl-0 p-0 mt-2">
                                <h4 class="font-bold timeline-header deep-orange white-text p-3 mb-0">We pay for working screens</h4>
                                <p class="mb-0 p-4">
                                    Once testing result report have been sent to you we
                                    will make payment to you by PayPal or Bank wife transfer.
                                </p>
                            </div>
                        </li>
                    </ul>
                    <!-- Timeline Wrapper -->

                </div>
            </div>
            <!-- Timeline -->
        </section>
    </div>

    <div class="container">
        <section id="contact-us" class="section pb-5">
            <h2 class="section-heading h1 pt-4">Contact Us</h2>
            <p class="section-description">
                We are always here to help you if you have any questions.
                Also you can use our Online Chat to recieve answer instantly. Feel free to usk us anything.
                Also use this form to leave feedback and we will publish here.
            </p>

            <div class="row">

                <!--Grid column-->
                <div class="col-md-8 col-xl-9">
                    <form action="contact_us.php" method="post" id="form-contact">
                        <!--Grid row-->
                        <div class="row">

                            <!--Grid column-->
                            <div class="col-md-6">
                                <div class="md-form">
                                    <div class="md-form">
                                        <input name="name" type="text" id="contact-name" class="form-control">
                                        <label for="contact-name" class="">Your name</label>
                                    </div>
                                </div>
                            </div>
                            <!--Grid column-->

                            <!--Grid column-->
                            <div class="col-md-6">
                                <div class="md-form">
                                    <div class="md-form">
                                        <input name="email" type="text" id="contact-email" class="form-control">
                                        <label for="contact-email" class="">Your email</label>
                                    </div>
                                </div>
                            </div>
                            <!--Grid column-->

                        </div>
                        <!--Grid row-->

                        <!--Grid row-->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="md-form">
                                    <input name="phone" type="text" id="contact-Phone" class="form-control" required maxlength="20">
                                    <label for="contact-Phone" class="">Your Phone (required)</label>
                                </div>
                            </div>
                        </div>
                        <!--Grid row-->

                        <!--Grid row-->
                        <div class="row">

                            <!--Grid column-->
                            <div class="col-md-12">

                                <div class="md-form">
                                    <textarea name="message" type="text" id="contact-message" class="md-textarea"></textarea>
                                    <label for="contact-message">Your message</label>
                                </div>

                            </div>
                        </div>
                        <!--Grid row-->

                        <div id='contact-recaptcha' class="g-recaptcha"
                             data-callback="sendContactFormAjax"
                             data-size="invisible"></div>

                        <div class="center-on-small-only">
                            <button class="btn btn-deep-orange" type="submit">Send</button>
                        </div>
                    </form>

                </div>
                <!--Grid column-->

                <!--Grid column-->
                <div class="col-md-4 col-xl-3">
                    <ul class="contact-icons">
                        <li><i class="fa fa-map-marker deep-orange-text  fa-2x"></i>
                            <p> 17105 North Bay Road b505 <br>Sunny Isles Beach, FL 33160, US</p>
                        </li>

                        <li><i class="fa fa-phone deep-orange-text  fa-2x"></i>
                            <p>+1 (786) 775 9337</p>
                        </li>

                        <li><i class="fa fa-envelope deep-orange-text fa-2x"></i>
                            <p>info@M2Bwholesale.com</p>
                        </li>
                    </ul>
                </div>
                <!--Grid column-->

            </div>

        </section>
    </div>
</main>

<!--Google map-->
<div id="map-container" class="z-depth-1" style="height: 500px"></div>

<footer class="page-footer center-on-small-only mdb-color">
    <div class="container">
        <div class="row my-4">
            <div class="col-md-4 col-lg-4">
                <h5 class="title mb-4 font-bold">M2Bwholesale</h5>
                <p>
                    If you arranging shipping on your own, please ship to address
                    given on the right and email us tracking number.
                </p>
                <p>
                    Shipping to us Benefits: <br/>
                    - Highest Prices in Market <br/>
                    - Buy good Lcd and bad Lcds <br/>
                    - 24 hour testing result <br/>
                    - 24 hours available by chat <br/>
                    - Transparent Reporting <br/>
                    - Fast Payments
                </p>
            </div>

            <hr class="clearfix w-100 d-md-none">

            <div class="col-md-3 col-lg-3 ml-auto">
                <h5 class="title mb-4 font-bold">Links</h5>
                <ul>
                    <p><a href="#">PRICES</a></p>
                    <p><a href="#">ABOUT US</a></p>
                    <p><a href="#">HOW IT WORKS</a></p>
                    <p><a href="#">REVIEWS</a></p>
                </ul>
            </div>

            <hr class="clearfix w-100 d-md-none">

            <div class="col-md-5 col-lg-4">
                <h5 class="title mb-4 font-bold">Adress</h5>
                <p><i class="fa fa-suitcase mr-3"></i> M2Bwholesale</p>
                <p><i class="fa fa-home mr-3"></i> 17105 North Bay Rd # B505</p>
                <p><i class="fa fa-globe mr-3"></i> Sunny Isles Beach, FL 33160, USA</p>
                <p><i class="fa fa-phone mr-3"></i> +1 (786) 775 9337</p>
                <p><i class="fa fa-envelope mr-3"></i> info@M2Bwholesale.com</p>
            </div>
            <hr class="clearfix w-100 d-md-none">
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            © 2018 Copyright: <a href="https://M2Bwholesale.com"> M2Bwholesale.com </a>
            <br>
            M2Bwholesale.com is in no way affiliated with the manufacturers of the items available for buyback. All
            logos, products & brands are owned by their respective companies. We only use these to identify for
            repair and do not claim any ownership or affiliation to these brand.
        </div>
    </div>
</footer>

<!-- JQuery -->
<script type="text/javascript" src="/js/jquery-3.2.1.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="/js/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="/js/mdb.min.js"></script>
<!-- Custom JS -->
<script type="text/javascript" src="/js/custom.js"></script>
<!-- Invisible Recaptcha -->
<script src="https://www.google.com/recaptcha/api.js?onload=recaptchaOnloadCallback&render=explicit"></script>
<!-- Pure Chat -->
<script type='text/javascript' data-cfasync='false'>window.purechatApi = { l: [], t: [], on: function () { this.l.push(arguments); } }; (function () { var done = false; var script = document.createElement('script'); script.async = true; script.type = 'text/javascript'; script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript'; document.getElementsByTagName('HEAD').item(0).appendChild(script); script.onreadystatechange = script.onload = function (e) { if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) { var w = new PCWidget({c: 'f1b2dcef-fe2b-4189-976b-8f7c5f4eb8cd', f: true }); done = true; } }; })();</script>
<!-- Yandex.Metrika -->
<script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter46970634 = new Ya.Metrika({ id:46970634, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true, trackHash:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/46970634" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- MailChimp -->
<script type="text/javascript" src="//downloads.mailchimp.com/js/signup-forms/popup/embed.js" data-dojo-config="usePlainJson: true, isDebug: false"></script><script type="text/javascript">require(["mojo/signup-forms/Loader"], function(L) { L.start({"baseUrl":"mc.us17.list-manage.com","uuid":"711f8b45b6936a604535bb8e5","lid":"42afe0dcbb"}) })</script>
<!-- Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-110957036-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-110957036-1');
</script>
<!--Google Maps-->
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDjmucSJWtACgZ3TorVX8K33rYLQ1YE4fQ&callback=initMap"></script>
</body>
</html>
