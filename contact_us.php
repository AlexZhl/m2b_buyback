<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

if ($_SERVER['REQUEST_METHOD'] != 'POST') {
    return;
}

require __DIR__ . '/includes/helpers.php';

$response = [
    'success' => false,
    'message' => '',
];

$recaptcha_solved = check_recaptcha();

if (!$recaptcha_solved) {
    $response['success'] = false;
    $response['message'] = 'Error. Invalid captcha. Please, try again';
    echo json_encode($response);
    return;
}

$form = [];
$form['name'] = htmlentities($_POST['name']);
$form['email'] = htmlentities($_POST['email']);
$form['phone'] = htmlentities($_POST['phone']);
$form['message'] = htmlentities($_POST['message']);

require __DIR__ . '/vendor/autoload.php';

// Send email to customer service
$parameters = [
    'to'         => [ 'Aatradeusa@yahoo.com' ],
    'subject'    => 'New Message From Customer ' . date("d.m.Y H:i"),
    'body'       => include 'includes/emails/email_customer_new_message.php',
];
send_email_swiftmailer($parameters);

$response = [
    'success' => true,
    'message' => 'Thank you. We will contact you as soon as possible',
];

// Send JSON response
echo json_encode($response);


try {
    $dbh = new PDO('mysql:host=localhost;dbname=buyback_data', 'm2bwhole69590821', 'Chicha2020@');
    $stmt = $dbh->prepare('INSERT INTO customer_messages (name, email, phone, message) VALUES (:name, :email, :phone, :message)');
    $stmt->bindParam(':name', $form['name']);
    $stmt->bindParam(':email', $form['email']);
    $stmt->bindParam(':phone', $form['phone']);
    $stmt->bindParam(':message', $form['message']);
    $stmt->execute();
} catch (\Exception $e) {
    $error_dump = var_export($e, true);
    $parameters = [
        'to'         => [ 'aatradeusa@yahoo.com' ],
        'bcc'        => [ 'alexzh1998@gmail.com' ],
        'subject'    => 'Unknown Error at Saving Contact Data to Database ' . date("d.m.Y H:i"),
        'body'       => include 'includes/emails/email_database_error.php',
    ];
    send_email_swiftmailer($parameters);
}
