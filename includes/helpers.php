<?php

require __DIR__ . '/../vendor/autoload.php';

function check_recaptcha()
{
    if (isset($_POST['g-recaptcha-response']) && $_POST['g-recaptcha-response']) {
        $data = [
            'secret'   => '6Ld4ZzwUAAAAAMkE0iAtBkA89hNbrPujXMMXNmQy',
            'response' => $_POST['g-recaptcha-response']
        ];

        $ch = curl_init('https://www.google.com/recaptcha/api/siteverify');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        $verifyResponse = curl_exec($ch);
        curl_close($ch);

        $responseData = json_decode($verifyResponse);
        return $responseData->success;
    } else {
        return false;
    }
}

function send_email_swiftmailer($parameters) {
    $mailer_host = 'mail.m2bwholesale.com';
    $mailer_port = '25';
//    $mailer_host = 'm2bwholesale.com';
//    $mailer_port = '465';
    $mailer_user = 'info@m2bwholesale.com';
    $mailer_password = 'hjg98jmf903jgkm0954jg45';

    if (!isset($parameters['from']) || !$parameters['from']) {
        $parameters['from'] = ['info@M2Bwholesale.com' => 'M2Bwholesale'];
    }

    $transport = (new Swift_SmtpTransport($mailer_host, $mailer_port))
        ->setUsername($mailer_user)
        ->setPassword($mailer_password)
    ;

    $mailer = new Swift_Mailer($transport);

    $message = (new Swift_Message())
        ->setFrom($parameters['from'])
        ->setTo($parameters['to'])
        ->setSubject($parameters['subject'])
        ->setBody($parameters['body'], 'text/html')
    ;
    if (isset($parameters['bcc']) && $parameters['bcc']) {
        $message->setBcc($parameters['bcc']);
    }
    if (isset($parameters['attachment']) && $parameters['attachment']) {
        $message->attach($parameters['attachment']);
    }
    if (isset($parameters['attachments']) && $parameters['attachments']) {
        foreach ($parameters['attachments'] as $attachment) {
            $message->attach($attachment);
        }
    }

    $result = $mailer->send($message);
}