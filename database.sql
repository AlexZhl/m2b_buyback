DROP TABLE IF EXISTS shippings;
DROP TABLE IF EXISTS shipping_labels;
DROP TABLE IF EXISTS customer_messages;

CREATE TABLE shippings
(
	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(255),
	company VARCHAR(255),
	email VARCHAR(400),
	phone VARCHAR(20),
	address_line_1 VARCHAR(36),
	address_line_2 VARCHAR(36),
	zip_code VARCHAR(6),
	city VARCHAR(255),
	state VARCHAR(255),
	country VARCHAR(2),

	boxes_quantity INT,

	iphone_x INT,
	iphone_8_plus INT,
	iphone_8 INT,
	iphone_7_plus INT,
	iphone_7 INT,
	iphone_6s_plus INT,
	iphone_6s INT,
	iphone_6_plus INT,
	iphone_6 INT,
	iphone_5 INT,

	total_quantity INT,

    approved BOOLEAN NOT NULL DEFAULT 0,
    approve_token VARCHAR(255),

	identification_number VARCHAR(50),

    created_at DATETIME DEFAULT CURRENT_TIMESTAMP
)
COLLATE = 'utf8_general_ci';

CREATE TABLE shipping_labels
(
	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	shipping_id INT,
	tracking_number VARCHAR(50),
	img VARCHAR(255),
    FOREIGN KEY (shipping_id) REFERENCES shippings(id) ON DELETE SET NULL,
	created_at DATETIME DEFAULT CURRENT_TIMESTAMP
)
COLLATE = 'utf8_general_ci';

CREATE TABLE customer_messages
(
	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(255),
	email VARCHAR(400),
	phone VARCHAR(20),
	message TEXT,
	created_at DATETIME DEFAULT CURRENT_TIMESTAMP
)
COLLATE = 'utf8_general_ci';
